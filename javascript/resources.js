
function openProjectModal(project){
	switch(project){
		case "capstone":
			var header = "<div class=project_title><p>Projects - Senior Capstone Design Project</p></div>";
			var body =  "<div class='project_specs'>"+
			"<div class='project_spec'><i class='fa fa-clock-o fa-2x' aria-hidden='true'></i><br>2016 - Current<br></div>"+
			"<div class='project_spec'><i class='fa fa-code fa-2x' aria-hidden='true'></i><br>HTML, CSS, <br>Javascript, Chrome API</div>"+
			"<div class='project_spec'><i class='fa fa-external-link fa-2x' aria-hidden='true'></i><br><a href='https://www.cefns.nau.edu/capstone/projects/CS/2017/AutomatedTesting/' target='_blank'>Team Website</a><br></div></div>"+
			"<div class='project_description'><p>As a 4th-year university student I have been involved in a two-semester capstone project. The goal of capstone was to follow a close-to-industry software development project. We got to choose a project from a list and were placed in a team of fellow Computer Science students.<br><br>I was placed in a team working on a project for Choice Hotels International’s IT group in Phoenix. The project involved their use of automated testing for all of Choice Hotels’ websites, specifically, creating a tool to help further automate their testing process. They use the testing suites Selenium and Jasmine. Under Choice’s previous method they used Firefox or Chrome browser's inspect element tool to manually pull all UI elements from a page they would like to develop tests for using one of the testing suites. We developed a Chrome extension that pulls all UI elements from a page and outputs a specific testing suite’s file type with the elements as objects. This means the process of testing was further automated for any testers using these testing suites. Choice uses Angular.js for development and so the tool works with Angular elements.<br><br>The first semester of the project was dedicated to starting the software development process and planning our project. We started with a formal requirements acquisition phase and technological feasibility document. We got the opportunity to visit the Choice Hotels office in Phoenix to see how their developers work and held a design session for the project. Soon after that we had our first design review presentation to the faculty mentors and fellow capstone teams. This was only one of many presentations we did during the first and second semesters. In the second semester we be conducted further design review presentations. We finished the project with a final presentation and finished product to our client, Choice Hotels. I was very pleased with the success of our project and Choice Hotels was also very satisfied. The project is avaliable for download on the Chrome Store.</p></div>";
			var footer = "";

			openModal(header, body, footer);
		break;

		case "taskplanner":
			var header = "<div class=project_title><p>Projects - MyTaskPlanner</p></div>";
			var body =  "<div class='project_specs'>"+
			"<div class='project_spec'><i class='fa fa-clock-o fa-2x' aria-hidden='true'></i><br>2015 - Current<br></div>"+
			"<div class='project_spec'><i class='fa fa-code fa-2x' aria-hidden='true'></i><br>HTML, CSS, PHP, AJAX<br>Javascript, mySQL<br>JQuery, Handlebars.js</div>"+
			"<div class='project_spec'><i class='fa fa-external-link fa-2x' aria-hidden='true'></i><br><a href='http://www.mytaskplanner.com' target='_blank'>mytaskplanner.com</a><br></div></div>"+
			"<div class='project_description'><p>MyTaskPlanner is a website in which users can create an account to keep track of tasks. Users can add, edit, remove, order, and track tasks.<br><br>I started developing Taskplanner in early 2015 after gaining an interest in web development from an introductory course. I wanted to take my education in web development further and learn how to create useful and beautiful sites. For this reason I decided to start without the use of frameworks so I could learn how to do everything myself. After the initial website was finished a few friends started using it and so I decided to continue development. I still continue to work on it as a side project, refactoring old code I wrote to be cleaner and more efficient and adding new features. I now am working with a fellow student developer to help me with the front end of the website.<br><br>Taskplanner has been a great experience both for education and fun. I’ve used the project as a way to further my skills with web programming languages, software development, using git repositories, and collaboration with other developers.</p></div>";
			var footer = "";

			openModal(header, body, footer);
		break;

		case "graphics":
			var header = "<div class=project_title><p>Projects - Computer Graphics</p></div>";
			var body =  "<div class='project_specs'>"+
			"<div class='project_spec'><i class='fa fa-clock-o fa-2x' aria-hidden='true'></i><br>Fall 2016<br></div>"+
			"<div class='project_spec'><i class='fa fa-code fa-2x' aria-hidden='true'></i><br>C</div>"+
			"<div class='project_spec'><i class='fa fa-external-link fa-2x' aria-hidden='true'></i><br><a href='https://gitlab.com/phaschke/CS430Project4' target='_blank'>Project Gitlab Page</a><br></div></div>"+
			"<div class='project_description'><p>I took a computer graphics course as a Computer Science elective for my degree (NAU Course CS430). In this class we learned about the basics of computer rendering, algorithms in computer graphics, images, raytracing, and raycasting, and I furthered my knowledge of C. I found this course to be particularly interesting with the projects we completed. Though a series of projects (2 - 4) written in C, we developed a very crude raytracer at the beginning which we added to through further projects.  At the end, we had a recursive raytracer that included sphere and plane objects, point and spot lights objects for illumination, and diffuse and specular object properties for reflections and refractions. In the fifth and final project we created a simple Image view that allowed to translate, rotate, scale, and shear an inputted .ppm image. This project used openGL and glfw. All projects are available on my gitlab profile.<br><br>This class provided an excellent basic education into the field of computer graphics and extended my knowledge of the C programming language.</p></div>";
			var footer = "";

			openModal(header, body, footer);
		break;

		case "portfolio":
			var header = "<div class=project_title><p>Projects - My Portfolio</p></div>";
			var body =  "<div class='project_specs'>"+
			"<div class='project_spec'><i class='fa fa-clock-o fa-2x' aria-hidden='true'></i><br>2017<br></div>"+
			"<div class='project_spec'><i class='fa fa-code fa-2x' aria-hidden='true'></i><br>HTML, CSS, PHP, AJAX<br>Javascript</div>"+
			"<div class='project_spec'><i class='fa fa-external-link fa-2x' aria-hidden='true'></i><br><a href='http://www.peterchaschke.com' target='_blank'>You're on it!</a><br></div></div>"+
			"<div class='project_description'><p>My portfolio website is a way to showcase my projects and relevant skills. I developed it using the skills I have gained from other projects.</p></div>";
			var footer = "";

			openModal(header, body, footer);
		break;

		default:
			alert("openPojectModal() function call on invalid project.");
	}
}

function submitMessage(){
	var firstName = document.getElementById("first_name").value;
	var lastName = document.getElementById("last_name").value;
	var email = document.getElementById("email").value;
	var message = document.getElementById("message").value;

	if(firstName.length == 0){
		colorField("first_name", "#FF6666");
		producePrompt("Name is required.", "message_feedback", "#FF6666");
		return false;
	}else{
		colorField("first_name", "#FFFFFF");
		producePrompt("", "message_feedback", "");
	}
	if(email.length == 0){
		colorField("email", "#FF6666");
		producePrompt("Email is required.", "message_feedback", "#FF6666");
		return false;
	}else{
		colorField("email", "#FFFFFF");
		producePrompt("", "message_feedback", "");
	}
	if(!validateEmail()){
		colorField("email", "#FF6666");
		producePrompt("Invalid email.", "message_feedback", "#FF6666");
		return false;
	}else{
		colorField("email", "#FFFFFF");
		producePrompt("", "message_feedback", "");
	}
	if(message.length == 0){
		colorField("message", "#FF6666");
		producePrompt("A message is required.", "message_feedback", "#FF6666");
		return false;
	}else{
		colorField("message", "#FFFFFF");
		producePrompt("", "message_feedback", "");
	}

	document.getElementById("submit_message").disabled = true;

	$.ajax({
		type: 'POST',
		url: 'php/sendMessage.php',		//<-----------------------------------------
		data: {func: 'submitMessage', 'firstName':firstName, 'lastName':lastName, 'email':email, 'message':message},
		dataType: 'json',

		success: function(data){
			if(data['returnValue'] == 0){	//Error in account creation
				//Failed
				alert(data['returnMessage']);
				return false;
			}else{	//No errors
				document.getElementById("first_name").value = "";
				document.getElementById("last_name").value = "";
				document.getElementById("email").value = "";
				document.getElementById("message").value = "";

				producePrompt("Message was successfully sent, thanks!", "message_feedback", "#5FB336");

				document.getElementById("submit_message").disabled = false;
			}
		}
	});
}

function validateEmail(){
	var email = document.getElementById("email").value;
	var regexEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

	if(email.length == 0){
		return false;
	}else{
		if(!email.match(regexEmail)){;
			return false;
		}else{
			return true;
		}
	}
}

function producePrompt(message, promptLocation, color){
	document.getElementById(promptLocation).innerHTML = message;
	document.getElementById(promptLocation).style.color = color;
}

function colorField(fieldLocation, color){
	document.getElementById(fieldLocation).style.background = color;
}

function colorBackground(backgroundLocation, color){
	document.getElementById(backgroundLocation).style.backgroundColor = color;
}

/**
 * Function openModal() opens a popup box with the internal css that was passsed in.
 * HeaderHTML, BodyHTML, and FooterHTML. Can pass in "" if you want any of the parts to be left blank.
 */
function openModal(headerHTML, bodyHTML, footerHTML){

	var modal = document.getElementById("modal");
	//Fill in the inputted HTML.
	document.getElementById("modalHeader").innerHTML = headerHTML;

	document.getElementById("modalBody").innerHTML = bodyHTML;

	document.getElementById("modalFooter").innerHTML = footerHTML;

	//Display the modal
	modal.style.display = "block";

	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
    	closeModal();
	};

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
    if (event.target == modal) {
        closeModal();
    	}
	};
}

/**
 * Function to close an opened modal.
 */
function closeModal(){
	var modal = document.getElementById("modal");
	modal.style.display = "none";
	return true;
}
