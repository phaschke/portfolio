<!DOCTYPE html>

<html>

	<link rel="stylesheet" type="text/css" href="css/defaultstyle.css"/>
	<link rel="stylesheet" type="text/css" href="css/indexstyle.css"/>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Peter Haschke</title>
        <link rel="shortcut icon" href="resources/images/p-64.ico" >
        <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="javascript/resources.js"></script>
        <script src="https://use.fontawesome.com/659b473aca.js"></script>
    </head>

    <body>
    	<div class = "wrapper">
    	<div class="nav_bar">
    			<div class="center1">
    				<div class="center2">
    			<ul class="tab">
    				<li><div class="nav_bar_img"><a href="http://www.peterchaschke.com"><img src="resources/images/p-64.png" alt="P Icon" ></a></div></li>
    				<li><a href="#about" class="tablinks">About</a></li>
    				<li><a href="#resume" class="tablinks">Resume</a></li>
    				<li><a href="#projects" class="tablinks">Projects</a></li>
    				<li><a href="#contact" class="tablinks">Contact</a></li>
    			</ul>
    			</div>
    			</div>
    		</div>


    	<div class="container">

    		<div class="header">

    				<div>
  						<img class="img-circle" src="resources/images/squareProfile(small).jpg" alt="Picture of me"/>
					</div>

    			<div class="title">
    				<p>Peter Haschke</p>
    			</div>


    		</div>



    		<hr>

    		<div class="content" id="about">

    			<div class="heading">About</div>
    				<p>
							Hello! My name is Peter Haschke. I graduated from Northern Arizona University in May 2017 with a bachelor's degree in Computer Science.
						</p>
						<p>
            	<b>I am currently seeking an opportunity to start my professional career in software engineering and development.</b>
						</p>
						<br>
						<p>
							I have a strong desire to always be learning new skills through solving real-life problems. I work well both working as an individual and with a group.
            	My passion is to create high-quality, secure, and maintainable software solutions.
            	My goal is always to design and create innovative and efficient code that is easily scalable. I am detail oriented and always seek the better way
            	to solve a problem.
						</p>
						<br>
						<p>
							Below you can find some of my projects and my r&eacutesum&eacute.
						</p>

    		</div>

    		<div class="colored_background">
    		<div class="content" id="resume">

    			<div class="heading">R&eacutesum&eacute</div>
    			<div class="grid_wrapper">
    				<div class="grid_box resume_grid_box" id="education">
    					<img class="icon" src="resources/images/GraduationCap-80.png" alt="Graduation Cap Icon"/><br>
    					<span class="medium-heading"><b>Education</b></span><br>
    					<p><b>Northern Arizona University</b><br>
    					<span class="light_blue_text"><b>Major:</b></span> B.S. Computer Science (2017)<br>
    					<span class="light_blue_text"><b>GPA:</b></span> 3.3<br>
    					<span class="light_blue_text"><b>Related Course Work:</b></span> Capstone,
    						<br>Networking, Operating Systems,
    						<br>Computer Graphics, Databases,
    						<br>Algorithms, Data Structures,
    						<br> Software Engineering</p>
    				</div>
    				<div class="grid_box resume_grid_box" id="skills">
    					<img class="icon" src="resources/images/Skills-80.png" alt="Skills Icon"/><br>
    					<span class="medium-heading"><b>Skills</b></span><br>
    					<p><span class="light_blue_text"><b>Languages:</b></span> Javascript, HTML, CSS,<br> PHP, C, Java, Python, AJAX<br>
							<span class="light_blue_text"><b>Concepts:</b></span> MVC Architecture, Object-Oriented Design,<br> Networking, Agile Methodologies<br>
							<span class="light_blue_text"><b>Operating Systems:</b></span> Linux, Windows<br>
    					<span class="light_blue_text"><b>Tools:</b></span> Git, Slack, Vim<br></p>
    				</div>
    			</div>
    			<div id="resume_button">
						<!--<input class="button" id="view_resume" type="submit" value="View Full Resume">-->
						<a href="resume.pdf" class="button" target="_blank" id="view_resume">View Full Resume</a>
				</div>
    		</div>
    		</div>


    		<div class="content" id="projects">
    			<div class="heading">Projects</div>

    			<p>The following are projects I have worked worked on. These include examples of projects from university classes and from personal projects.
    				Each box can be clicked on to view more detailed information about each.</p>

    			<div class="grid_wrapper">
    				<div class="grid_box project_grid_box" onclick="openProjectModal('capstone')">
    					<div class="project_image"><img src="resources/images/project_capstone.jpg" alt="Capstone Screenshot" ></div>
    					<div class="grid_box_label">Senior Capstone Project</div>
    				</div>
    				<div class="grid_box project_grid_box" onclick="openProjectModal('taskplanner')">
    					<div class="project_image"><img src="resources/images/project_task_planner.jpg" alt="Taskplanner Screenshot"></div>
    					<div class="grid_box_label">MyTaskPlanner</div>
    				</div>
    				<div class="grid_box project_grid_box" onclick="openProjectModal('graphics')">
    					<div class="project_image"><img src="resources/images/project_graphics.jpg" alt="Graphics Screenshot"></div>
    					<div class="grid_box_label">Computer Graphics Course</div>
    				</div>
    				<div class="grid_box project_grid_box" onclick="openProjectModal('portfolio')">
    					<div class="project_image"><img src="resources/images/project_portfolio.jpg" alt="Portfolio Screenshot"></div>
    					<div class="grid_box_label">Portfolio Website</div>
    				</div>
    			</div>
    		</div>


    		<div class="colored_background">
    		<div class="content" id="contact">
    			<div class="heading">Contact Me</div>
    			<div><a href='https://www.linkedin.com/in/peter-haschke-89618b10a?trk=hp-identity-name' target='_blank'><img class="icon" src="resources/images/LinkedIn-60.png" title="Visit My LinkedIn Profile" alt="LinkedIn Icon"/></a>
    				<a href='https://gitlab.com/phaschke/' target='_blank'><img class="icon" src="resources/images/GitLab-60.png" title="Visit My Gitlab Profile" alt="GitLab Icon"/></a>
    				<a href='https://github.com/phaschke' target='_blank'><img class="icon" src="resources/images/GitHub-60.png" title="Visit My Github Profile" alt="Github Icon"/></a>
    			</div>
    			<p>I would love to hear from you! Send me a message below and I'll get back to you as soon as possible!</p>
    			<div id="contact_form">
    				<div>
						<b><label for="first_name">First Name<em>*</em>:</label></b>
						<input type="text" id="first_name" placeholder="Enter Your First Name" autocomplete="off" maxlength="50"/>
					</div>
					<div>
						<b><label for="last_name">Last Name:</label></b>
						<input type="text" id="last_name" placeholder="Enter Your Last Name" autocomplete="off" maxlength="50"/>
					</div>
					<div>
						<b><label for="email">Email<em>*</em>:</label></b>
						<input type="text" id="email" placeholder="Enter Your Email" autocomplete="off" maxlength="128"/>
					</div>
					<div>
						<b><label for="message">Message<em>*</em>:</label></b>
						<textarea id="message" placeholder="Enter Your Message" rows="5" maxlength="1000"></textarea>
					</div>
					<div class="message_feedback">
						<label id="message_feedback"></label>
					</div>
					<div id="submit_button">
						<input class="button" id="submit_message" type="submit" onclick="submitMessage()" value="Submit Message">
					</div>
				</div>
    		</div>
    		</div>

    	</div>





		</div>
		<footer class="footer">
    			<p>&copy;2017 Peter Haschke</p>
		</footer>

		<!-- MODAL DIV -->
		<div id="modal" class="modal">

  			<!-- Modal content -->
			<div class="modal-content">
  				<div class="modal-header">
    				<span class="close">×</span>
    				<div id="modalHeader"></div>
  				</div>

  				<div class="modal-body" id="modalBody">

  				</div>

  				<div class="modal-footer" id="modalFooter">
  				</div>
			</div>

		</div>

    </body>

</html>
